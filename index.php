<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 07.10.2020
 * Time: 0:15
 */
require_once("src/php/config.php");

$OUTPUT->setTitle('О проекте');

echo $OUTPUT->header();

$templatePath = ($_COOKIE['lang']=='en')?'src/template/en/index.html':'src/template/29/index.html';
$content = file_get_contents($templatePath);

$content = replaceLink($content);
$content = replaceMainCard($content);
$content = replaceScienInt($content);
$content = replaceExpertList($content);

echo $content;

echo "<script src='src/js/timer.js'></script>";

echo $OUTPUT->footer();


function replaceMainCard($content){
    global $DB;

    $cards = $DB->getRecordsSql("select id, type, title, text from bls_captions");

    foreach($cards as $card){
        $content = str_replace("#$card->type#", $card->text, $content);
    }

    return $content;
}

function replaceScienInt($content){
    global $DB;

    $scienItem = $DB->getRecordsSql("select id, text from bls_scientific_interests where unused = 0");
    $scienText = '';

    foreach($scienItem as $item){
        $scienText .= "<div class=\"directions__item\">$item->text</div>";
    }

    $content = str_replace('#scienInt#', $scienText, $content);

    return $content;
}

function replaceExpertList($content){

    $cardTemplate = file_get_contents('src/template/29/expertCard.html');

    $text = [
        1 => "<h3>Донник Ирина Михайловна<br></h3> Вице-президент РАН, академик РАН, Руководитель НПП 3 НОЦ \"Селекционно-генетические исследования, клеточные технологии и генная инженерия: животноводство\"",
        2 => "<h3>Кирпичников Михаил Петрович<br></h3> российский учёный в области физико-химической биологии, белковой инженерии и биотехнологии; государственный и общественный деятель декан биологического факультета МГУ им. М.В. Ломоносова, академик РАН",
        3 => "<h3>Галстян Арам Генрихович<br></h3> Врио директора ФГАНУ «ВНИМИ», Академик РАН, руководитель НПП 4 НОЦ \"Здровьесберегающие технологии: производство продовольствия и ветпрепаратов\"",
        4 => "<h3>Кульчин Юрий Николаевич<br></h3> д.физ.н., директор Института автоматики и процессов управления ДВО РАН академик РАН"
    ];

    $search = [
        "#src#",
        "#text#"
    ];

    $expertCard = '';
    $expertCard .= '<hr>';
    foreach ($text as $key => $item){

        $path = "src/data/expert/" . $key . "_cut.jpg";
        if(file_exists($path)){
            $expertCard .= str_replace($search, [$path, $item], $cardTemplate);
            if (next($text)){
                $expertCard .= '<hr>';
            }
        }
    }

    $content = str_replace('#expertList#', $expertCard, $content);

    return $content;
}

function replaceLink($content){

    if(isLogin()){
        $link =  "thesis.php";
    } else {
        $link = "registration.php";
    }

    $content = str_replace('#linkLocation#', $link, $content);

    return $content;
}