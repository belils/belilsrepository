    <!-- HEAD -->
    <?php require "src/elements/head.php"; ?>

    <div class="wrapper">

        <div class="content">
        <?php require "src/elements/header.php"; ?>

            <div class="main">
                <!-- <div class="container_main"> -->


                <hr>

                <!-- INDEX__CONTENT -->
                <div class="title_project">
                    <div class="container">
                        <div class="project__body">
                            <div class="project__title title">
                                <h2>Заголовок о проекте конференции</h2>
                            </div>
                            <div class="project__text text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, necessitatibus quis.
                                Corporis
                                quaerat repellendus et odio id, aspernatur similique molestiae dolores quae alias,
                                consectetur iure beatae ex? Recusandae, laudantium eius.
                            </div>
                        </div>

                        <hr>

                        <div class="whom__body">
                            <div class="whom__title title">
                                <h2>Заголовок для кого</h2>
                            </div>
                            <div class="whom__text text">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, necessitatibus quis.
                                Corporis
                                quaerat repellendus et odio id, aspernatur similique molestiae dolores quae alias,
                                consectetur iure beatae ex? Recusandae, laudantium eius.
                            </div>
                        </div>

                        <hr>

                        <div class="directions__body">
                            <div class="directions__title title">
                                <h2>Заголовок о направлениях</h2>
                            </div>
                            <div class="directions__items">
                                <div class="item">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi soluta alias
                                    recusandae
                                    rerum velit illum, tempore cum provident suscipit qui amet incidunt iusto eum
                                    blanditiis
                                    esse. Aut est nobis deleniti.
                                </div>
                                <div class="item">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi soluta alias
                                    recusandae
                                    rerum velit illum, tempore cum provident suscipit qui amet incidunt iusto eum
                                    blanditiis
                                    esse. Aut est nobis deleniti.
                                </div>
                                <div class="item">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi soluta alias
                                    recusandae
                                    rerum velit illum, tempore cum provident suscipit qui amet incidunt iusto eum
                                    blanditiis
                                    esse. Aut est nobis deleniti.
                                </div>
                                <div class="item">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi soluta alias
                                    recusandae
                                    rerum velit illum, tempore cum provident suscipit qui amet incidunt iusto eum
                                    blanditiis
                                    esse. Aut est nobis deleniti.
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="expert__body">
                            <div class="expert__title title">
                                <h2>Заголовок об экспертах</h2>
                            </div>
                            <div class="expert__items">
                                <div class="expert_item">
                                    <div class="item__photo">
                                        <img src="https://static.tildacdn.com/tild3636-3735-4533-b238-633338306565/7350jpg_wh860.jpg"
                                            alt="">
                                    </div>
                                    <div class="item_fio">
                                        <h4>Рудаков Егор Сергеевич</h4>
                                    </div>
                                    <div class="item_text">
                                        1С программист
                                    </div>
                                </div>

                                <div class="expert_item">
                                    <div class="item__photo">
                                        <img src="https://static.tildacdn.com/tild3636-3735-4533-b238-633338306565/7350jpg_wh860.jpg"
                                            alt="">
                                    </div>
                                    <div class="item_fio">
                                        <h4>Нагайченко Владислав Эдуардович</h4>
                                    </div>
                                    <div class="item_text">
                                        JS программист
                                    </div>
                                </div>

                                <div class="expert_item">
                                    <div class="item__photo">
                                        <img src="https://static.tildacdn.com/tild3636-3735-4533-b238-633338306565/7350jpg_wh860.jpg"
                                            alt="">
                                    </div>
                                    <div class="item_fio">
                                        <h4>Настарчук Валерия Игоревна</h4>
                                    </div>
                                    <div class="item_text">
                                        Преподаватель
                                    </div>
                                </div>

                                <div class="expert_item">
                                    <div class="item__photo">
                                        <img src="https://static.tildacdn.com/tild3636-3735-4533-b238-633338306565/7350jpg_wh860.jpg"
                                            alt="">
                                    </div>
                                    <div class="item_fio">
                                        <h4>Жукова Вероника Сергеевич</h4>
                                    </div>
                                    <div class="item_text">
                                        Заведующий центром
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>
            <!-- MAIN -->
            </div>
            <!-- CONTENT -->
        </div>


        <!-- MODAL -->
        <?php require "src/elements/modal.php"; ?>


        <!-- XHR -->
        <script src="src/js/xhr.js"></script>


    <!-- FOOTER -->
    <?php require "src/elements/footer.php"; ?>

    </div>