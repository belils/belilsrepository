<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 25.09.2020
 * Time: 23:31
 */
require_once('src/php/config.php');

$edit = optional_param('edit', 0);

require_login();

$OUTPUT->setTitle('Личный кабинет');

echo $OUTPUT->header();


$path = $edit?"src/template/personalAreaEdit.html":"src/template/29/personalArea.html";

$personalArea = file_get_contents($path);


$personalData = $DB->getRecordSql("select userid, username, surname, idposition,
                                      birthday, city, organization,
                                      p.text as position, degree, email, phone, f.file_path as filepath
                                      from bls_user u 
                                      left join bls_position p on p.id = u.idposition
                                      left join bls_files f on f.id = u.pictureid
                                      where userid = " . $_SESSION['userid']);

if ($edit) {
    $optionPositionList = $DB->getRecordsSql("select id, text from bls_position");
    $optionPosition = '';

    foreach ($optionPositionList as $item) {
        $optionPosition .= "<option value='$item->id' ". (($item->id == $personalData->idposition)?'selected':'')  ." >$item->text</option>";
    }

    $personalArea = str_replace('#optionPosition#', $optionPosition, $personalArea);
}

$thesisList = returnThesisList();

$personalArea = str_replace('#thesisList#', $thesisList, $personalArea);

$personalArea = str_replace('#userid#', $USER->id, $personalArea);

$search = [
    '#imgPath#',
    '#username#',
    '#surname#',
    '#birthday#',
    '#city#',
    '#organization#',
    '#position#',
    '#degree#',
    '#email#',
    '#phone#'
];

$replace = [
    $personalData->filepath,
    $personalData->username,
    $personalData->surname,
    $personalData->birthday,
    $personalData->city,
    $personalData->organization,
    $personalData->position,
    $personalData->degree,
    $personalData->email,
    $personalData->phone
];

$personalArea = str_replace($search, $replace, $personalArea);

$cvalifDocs = $DB->getRecordsSql("SELECT s.id, f.file_path FROM bls_skill s
                                    inner join bls_files f on s.fileid = f.id
                                    WHERE s.userid = $USER->id");
$cvalifText = '';

$count = 1;
foreach ($cvalifDocs as $value) {
    $cvalifText .= "$count. <a href='src/php/download.php?filePath=$value->file_path'>Загрузить</a><br>";
    $count++;
}

$personalArea = str_replace('#listCvalif#', $cvalifText, $personalArea);

echo $personalArea;

echo "<script src='src/js/timer.js'></script>";


echo $OUTPUT->footer();


function returnThesisList(){
    global $DB, $USER;

    $result = '';

    $thesisList = $DB->getRecordsSql("select t.thesis_id, from_unixtime(date_app, '%d.%m.%Y') as dateapp, si.text, 
                                        tt.thesistext, f.file_path
                                        from bls_thesis t
                                        inner join bls_thesis_type tt on t.id_thesis_type = tt.id_thesis_type
                                        left join bls_scientific_interests si on t.id_scienint = si.id
                                        left join bls_thesis_info ti on ti.id_thesis = t.thesis_id
                                        inner join bls_files f on t.file_id = f.id
                                        where t.user_id = $USER->id");


    foreach ($thesisList as $thesis){
        $result .= "<b>Область научных интересов:</b> $thesis->text<br>";
        $result .= "<b>Форма участия:</b> $thesis->thesistext<br>";
        $result .= "<b>Дата подачи:</b> $thesis->dateapp<br>";
        $result .= "<b>Прикрепленный тезис:</b>" . createLinkFile($thesis->file_path);
        $result .= "<a href='src/php/deleteThesis.php?thesisid=$thesis->thesis_id'>Отозвать участие</a>";
        $result .= "<br>";
    }

    return $result;
}
