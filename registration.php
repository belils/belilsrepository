<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 29.09.2020
 * Time: 21:57
 */
require_once('src/php/config.php');

$OUTPUT->setTitle('Регистрация');

echo $OUTPUT->header();

$template = "./src/template/29/registration.html";

$optionPositionList = $DB->getRecordsSql("select id, text from bls_position");
$optionPosition = '';

foreach($optionPositionList as $item){
    $optionPosition .= "<option value='$item->id'>$item->text</option>";
}

$content = file_get_contents($template);

$content = str_replace('#optionPosition#', $optionPosition, $content);

echo $content;

echo $OUTPUT->footer();