<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 07.10.2020
 * Time: 22:57
 */
require_once('src/php/config.php');

$edit = optional_param('edit', 0);

require_login();

$OUTPUT->setTitle('Личный кабинет');
echo $OUTPUT->header();


if ($_COOKIE['lang']=='en'){
//    $path = $edit?"src/template/personalAreaEditEn.html":"src/template/29/en/personalArea2.html";
    $path = $edit?"src/template/personalAreaEdit.html":"src/template/en/personalArea2.html";
} else {
    $path = $edit?"src/template/personalAreaEdit.html":"src/template/29/personalArea2.html";
}
$content = file_get_contents($path);

$personalData = getPersonalData();

$content = replacePersonalData($content);
$content = replaceUserID($content);
if($edit){
    $content = replaceOptionPosition($content);
}
$content = replaceSubmitButton($content);
$content = replaceCvalifFiles($content);
$content = replaceThesis($content);


echo $content;
echo "<script src='src/js/timer.js'></script>";
echo $OUTPUT->footer();


function replacePersonalData($content){
    global $personalData;

    $search = [
        '#imgPath#',
        '#username#',
        '#surname#',
        '#birthday#',
        '#city#',
        '#organization#',
        '#position#',
        '#degree#',
        '#email#',
        '#phone#'
    ];

    $replace = [
        (!is_null($personalData->filepath)?$personalData->filepath:"src/data/img/photoDefault.png"),
        $personalData->username,
        $personalData->surname,
        $personalData->birthday,
        $personalData->city,
        $personalData->organization,
        $personalData->position,
        $personalData->degree,
        $personalData->email,
        $personalData->phone
    ];

    $content = str_replace($search, $replace, $content);

    return $content;
}

function replaceUserID($content){
    global $USER;

    $content = str_replace('#userid#', $USER->id, $content);

    return $content;
}

function getPersonalData(){
    global $DB;

    $personalData = $DB->getRecordSql("select userid, username, surname, idposition,
                                      birthday, city, organization,
                                      p.text as position, degree, email, phone, f.file_path as filepath
                                      from bls_user u 
                                      left join bls_position p on p.id = u.idposition
                                      left join bls_files f on f.id = u.pictureid
                                      where userid = " . $_SESSION['userid']);

    return $personalData;
}

function replaceOptionPosition($content){
    global $DB, $personalData;

    $optionPositionList = $DB->getRecordsSql("select id, text from bls_position");
    $optionPosition = '';

    foreach ($optionPositionList as $item) {
        $optionPosition .= "<option value='$item->id' ". (($item->id == $personalData->idposition)?'selected':'')  ." >$item->text</option>";
    }

    $content = str_replace('#optionPosition#', $optionPosition, $content);

    return $content;
}

function replaceCvalifFiles($content){
    global $DB, $USER;

    $cvalifDocs = $DB->getRecordsSql("SELECT s.id, f.file_path FROM bls_skill s
                                    inner join bls_files f on s.fileid = f.id
                                    WHERE s.userid = $USER->id");
    $cvalifText = '';

//    $count = 1;
    if(count($cvalifDocs) > 0) {
        foreach ($cvalifDocs as $value) {
            if ($_COOKIE['lang'] == 'en'){
                $link = "<a href='src/php/download.php?filePath=$value->file_path'>Document (download)</a>";
            } else {
                $link = "<a href='src/php/download.php?filePath=$value->file_path'>Документ (загрузить)</a>";
            }
            $cvalifText .= "<li><a href='src/php/deleteDoc.php?id=$value->id'><img  class=\"ic\" src=\"https://img.icons8.com/material-outlined/24/000000/delete-trash.png\"></a>$link</li>";
        }
    } else {
        if ($_COOKIE['lang'] == 'en'){
            $cvalifText = "<h6>Files not loaded</h6>";
        } else {
            $cvalifText = "<h6>Файлы не загружены</h6>";
        }
    }

    $content = str_replace('#listCvalif#', $cvalifText, $content);


    return $content;
}

function replaceThesis($content){
    global $DB, $USER;

    $thesisCards = '';
    //добавить inner join bls_thesis_file
    $cardTemplate = file_get_contents("src/template/29/thesisCard.html");
    $thesisList = $DB->getRecordsSql("select t.thesis_id, from_unixtime(date_app, '%d.%m.%Y') as dateapp, si.text, 
                                        tt.thesistext
                                        from bls_thesis t
                                        inner join bls_thesis_type tt on t.id_thesis_type = tt.id_thesis_type
                                        left join bls_scientific_interests si on t.id_scienint = si.id
                                        left join bls_thesis_info ti on ti.id_thesis = t.thesis_id
                                        where t.user_id = $USER->id");


    if(count($thesisList) > 0){

        $search = [
            "#date#",
            "#scienint#",
            "#form#",
            "#linkDownload#",
            "#linkReject#"
        ];

        foreach ($thesisList as $item){

            $replace = [
                $item->dateapp,
                $item->text,
                $item->thesistext,
//                "/src/php/download.php?filePath=$item->file_path",
                "/src/php/downloadThesisToZip.php?thesisid=$item->thesis_id",
                "src/php/deleteThesis.php?thesisid=$item->thesis_id"
            ];
            $thesisCards .= str_replace($search, $replace, $cardTemplate);
        }
    } else {
        if ($_COOKIE['lang'] == 'en'){
            $thesisCards = "<div class=\"thesis__row\" style='text-align: center'><h3>Thesis not loaded</h3></div>";
        } else {
            $thesisCards = "<div class=\"thesis__row\" style='text-align: center'><h3>Тезисы не загружены</h3></div>";
        }
    }

    $content = str_replace('#thesisList#', $thesisCards, $content);

    return $content;
}

function replaceSubmitButton($content){

//    <button type="button" class=" col-md-12 btnNew" onclick="window.location.href = 'thesis.php';">Подать тезис</button>
    $search = '#submitThesis#';
    if ($_COOKIE['lang'] == 'en') {
        $replace = "<button type='button' class='col-md-12 btnNew' onclick=\"window.location.href = 'thesis.php?thesisform=1';\">Submit thesis</button>";
    } else {
        $replace = "<button type='button' class='col-md-12 btnNew' onclick=\"window.location.href = 'thesis.php?thesisform=1';\">Подать тезис</button>";
    }
    if (isStudent()){
        if ($_COOKIE['lang'] == 'en') {
            $replace .= "<button type=\"button\" class=\" col-md-12 btnNew\" onclick=\"window.location.href = 'thesis.php?thesisform=2';\">Submit project</button>";
        } else {
            $replace .= "<button type=\"button\" class=\" col-md-12 btnNew\" onclick=\"window.location.href = 'thesis.php?thesisform=2';\">Подать проект</button>";
        }
    }

    $content = str_replace($search, $replace, $content);

    return $content;
}