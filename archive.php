<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 22.09.2020
 * Time: 21:57
 */

require_once("src/php/config.php");

$year = optional_param('year', null);

$OUTPUT->setTitle('Архив');

echo $OUTPUT->header();


//$templatePath = ($_COOKIE['lang']=='en')?'src/template/en/index.html':'src/template/29/index.html';


$content = file_get_contents('src/template/29/archive.html');
$templatePath = ($_COOKIE['lang']=='en')?'src/template/en/archive.html':'src/template/29/archive.html';
$content = file_get_contents($templatePath);

if(isset($year)){
    $content = replaceArchive($content);
}

echo $content;

echo $OUTPUT->footer();

function returnArhiveSelect(){

    $yearStart = 2015;
    $yearEnd = date('Y', time());

    $result = "<select name='town' id='s1'>";
    $result .= "<option value='0'>Выберите год</option>";

    for($i = $yearStart; $i<=$yearEnd; $i++){
        $result .= "<option class='optionss' value='$i' >$i</option>";
    }

    return $result . "</select>";
}

function replaceArchive($content){
    global $DB, $year;

    $archiveText = '';
    //добавить inner join bls_thesis_file
    $listArchive = $DB->getRecordsSql("select t.thesis_id, concat(u.surname, ' ', u.username) as username, si.text,
                        tt.thesistext
                        from bls_thesis t
                        inner join bls_thesis_type tt on tt.id_thesis_type = t.id_thesis_type
                        left join bls_thesis_info ti on ti.id_thesis = t.thesis_id
                        inner join bls_user u on t.user_id = u.userid
                        inner join bls_scientific_interests si on si.id = t.id_scienint
                        where from_unixtime(date_app,'%Y') = $year and t.user_id <> 1");


    if (count($listArchive)>0){
        foreach ($listArchive as $item){
//            $archiveText .= "<li class='ols'><b>$item->username</b>, $item->text. <br> $item->thesistext форма участия. <a href='src/php/download.php?filePath=$item->file_path'>Скачать документ</a></li>";


//            $archiveText .= "<li class='ols'><b>$item->username</b>, $item->text. <br> $item->thesistext форма участия. <a href='src/php/downloadThesisToZip.php?thesisid=$item->thesis_id'>Скачать документ(ы)</a></li>";

            if ($_COOKIE['lang']=='en') {

                $archiveText .= "<li class='ols'><b>$item->username</b>, $item->text. <br> $item->thesistext Form of participation <a href='src/php/downloadThesisToZip.php?thesisid=$item->thesis_id'>download documents</a></li>";
            } else {
                $archiveText .= "<li class='ols'><b>$item->username</b>, $item->text. <br> $item->thesistext форма участия. <a href='src/php/downloadThesisToZip.php?thesisid=$item->thesis_id'>Скачать документ(ы)</a></li>";
            }
            if (next($listArchive)){
                $archiveText .= '<hr>';
            }
        }
    } else {
        $archiveText = "<h3 style='text-align:center; padding-top: 100px; padding-bottom: 230px;'>Нет тезисов</h3>";
    }

    $content = str_replace("#listThesis#", $archiveText, $content);

    return $content;
}
