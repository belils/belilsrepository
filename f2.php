<?php

require_once __DIR__ . '/src/php/config.php';

$frm = (object)$_POST;

if(isset($frm->send)){

    $path = upload_file('file', 'src/data/thesis/');


    if ($path) {
        $rec = new stdClass();


        $rec->type = 'type';
        $rec->file_path = $path;
        $rec->timemodified = time();
        $id = $DB->insertRecord('bls_files', $rec);

        //thesis
        if($id){
            $rec2 = new stdClass();
            $rec2->id_thesis_type = $frm->thesisType;
            $rec2->date_app = time();
            $rec2->file_id = $id;
            $rec2->user_id = 1;
            $id = $DB->insertRecord('bls_thesis',$rec2);

            if($id) {
                //thesis_info
                $rec3 = new stdClass();
                $rec3->arrival_date = strtotime($frm->date);
                $rec3->id_thesis = $id;
                $rec3->transfer = $frm->trans;
                $rec3->coffee = ($frm->ch1 == 'true')?1:0;
                $rec3->lunch = ($frm->ch2 == 'true')?1:0;
                $rec3->dinner = ($frm->ch3 == 'true')?1:0;
                $DB->insertRecord('bls_thesis_info',$rec3);


                $sql = "select t.thesis_id, u.username,from_unixtime(date_app, '%d.%m.%Y') as dateapp, 
                        tt.thesistext, f.file_path
                        from bls_thesis t
                        inner join bls_thesis_type tt on tt.id_thesis_type = t.id_thesis_type
                        left join bls_thesis_info ti on ti.id_thesis = t.thesis_id
                        inner join bls_files f on f.id = t.file_id
                        inner join bls_user u on t.user_id = u.userid";

                $list = $DB->getRecordsSql($sql);
                echo json_encode(['list' => $list,
                    'status' => 'ok']);
            } else {
                echo json_encode(['status' => 'Error: thesis_type not found']);
            }

        } else {
            echo json_encode(['status'=>'Error: file not found']);
        }

    }
}



