<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 15.10.2020
 * Time: 13:54
 */
require_once('config.php');

$zip = new ZipArchive();
$filename = 'Archive2020.zip';

if($zip->open($filename, ZipArchive::CREATE) !== true){
    print_object('error');
    exit;
}

$listFiles = $DB->getRecordsSql("select t.thesis_id, concat(u.surname, ' ', u.username) as username, si.text, f.file_path, u.idposition, t.user_id
                                    from bls_thesis t
                                    inner join bls_thesis_type tt on tt.id_thesis_type = t.id_thesis_type
                                    # left join bls_thesis_info ti on ti.id_thesis = t.thesis_id
                                    inner join bls_thesis_file tf on tf.thesisid = t.thesis_id
                                    inner join bls_files f on f.id = tf.fileid
                                    inner join bls_user u on t.user_id = u.userid
                                    inner join bls_scientific_interests si on si.id = t.id_scienint
                                    where from_unixtime(date_app,'%Y') = 2020 order by t.user_id, t.thesis_id");

$thesisNumber = 0;
$lastThesisId = -1;
$lastUserId = -1;
foreach($listFiles as $file){

    if ($lastThesisId != $file->thesis_id){
        $thesisNumber++;
    }
    if ($lastUserId != $file->user_id){
        $thesisNumber = 1;
    }
    $lastThesisId = $file->thesis_id;
    $lastUserId = $file->user_id;


    $nameFile = end(explode('/', $file->file_path));
    $pathThesis = ($file->idposition <=3)?'Проекты/':'Тезисы/'; 
    $pathThesis .= $file->text . '/' . $file->username . '/' . $thesisNumber . '/' . $nameFile;
    // print_object($dirroot . $file->file_path);
    $zip->addFile($dirroot . $file->file_path, $pathThesis);
}

$zip->close();

if (file_exists($filename)) {
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    readfile($filename);
}

unlink($filename);