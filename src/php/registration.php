<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 18.09.2020
 * Time: 19:50
 */
require_once('config.php');

//header('Content-Type','application/x-www-form-urlencoded');

$frm = data_submitted();


$err = [];

$sql = "SELECT userid FROM bls_user WHERE login='$frm->login'";
$user = $DB->getRecordSql($sql);

if(isset($user->userid)){

    $err[] = "Пользователь с таким логином уже существует в базе данных";
} else if(count($err) == 0){

    $record = new stdClass();

    $record->username = $frm->surname;
    $record->surname = $frm->name;
    $record->login = $frm->login;
    $record->userpass = md5(trim($frm->password));
    $record->birthday = $frm->brth;
    $record->country = $frm->country;
    $record->city = $frm->city;
    $record->organization = $frm->organization;
    $record->degree = $frm->power;
    $record->idposition = $frm->position;
//    $record->scienint = $frm->hobby; // Старое
    $record->email = $frm->email;
    $record->phone = $frm->number;

//    print_object($record); exit;

    $userid = $DB->insertRecord('bls_user', $record);

    if(!is_null($userid)) {
        $userRole = new stdClass();

        $userRole->roleid = 2;
        $userRole->userid = $userid;
        $userRole->timemodified = time();
        $userRole->modifierid = $userid;

        $DB->insertRecord('bls_role_assignments', $userRole);
    }



    header("Location: ../../index.php");
//    echo json_encode(['status' => 'OK']);

} else {

    print "<b>При регистрации произошли следующие ошибки:</b><br>";

    foreach($err AS $error){

        print $error."<br>";
    }
}