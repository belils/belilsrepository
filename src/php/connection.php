<?php 

/**
  * 
  */
class DataBase {

    private $host = 'localhost';
    private $database = 'u607529909_belils';
    private $user = 'u607529909_root';
    private $password = 'xpTSyooTT18';
    private $charset = 'utf8';

	public function __construct(){
		// empty
	}

	public function getRecordsSql($sql){
	    $res = $this->executeQuery($sql);
	    for($i = 0; $i < count($res); $i++){
	        $res[$i] = (object) $res[$i];
        }

	    return $res;
//		return (object) $this->executeQuery($sql);
	}

	public function getRecordSql($sql){
		return (object) $this->executeQuery($sql)[0];
	}

	public function insertRecord($table, $obj){
		
		$name = [];
		$values = [];

		foreach ($obj as $key => $value) {
			if(gettype($value) == 'string'){
				$value = "'" . $value . "'";
			}
			array_push($name, $key);
			array_push($values, $value);
		}
		$sql = "insert into " . $table;
		$sql .= '(' . implode(',', $name) . ') values(';
		$sql .=  implode(',', $values) . ')';

		return $this->executeQuery($sql);
	}

	public function updateRecord($table, $obj){

	    $setWhere = false;
	    $where = '';

        $sql = "update $table set ";

        foreach ($obj as $key => $value) {
            if(!$setWhere){
                if (gettype($value) == 'string') {
                    $where = "where $key = '$value'";
                } else {
                    $where = "where $key = $value";
                }
                $setWhere = true;
                next($obj);
            } else {

                if (gettype($value) == 'string') {
                    $sql .= "$key = '$value'";
                } else {
                    $sql .= "$key = $value";
                }

                if (next($obj) !== false) {
                    $sql .= ', ';
                }
            }

        }

        $sql = "$sql $where";

        return $this->executeQuery($sql);
    }

	public function deleteRecordOnID($table, $where){
	    $sql = "delete from $table where $where";
	    $this->executeQuery($sql);
    }

	public function executeQuery($query){
		$link = mysqli_connect($this->host, $this->user, $this->password, $this->database) or die('Ошибка ' . mysqli_error($link));
		mysqli_set_charset($link, $this->charset);

		$result = mysqli_query($link, $query) or die('Ошибка ' . mysqli_error($link));

        if($result !== true){
            $result = mysqli_fetch_all($result, MYSQLI_ASSOC);
        } else {
            $result = mysqli_insert_id($link);
        }

		mysqli_close($link);
		return $result;
	}

	public function clearTable($table){
		$this->executeQuery("truncate " . $table);
	}

}