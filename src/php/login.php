<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 26.09.2020
 * Time: 12:18
 */

require_once('config.php');

$action = $_GET['action'];

$login = $_POST['login'];
$password = $_POST['userpass'];


if($action == 'login'){

    $user = $DB->getRecordSql("select userid, login, idposition from bls_user where login = '$login' and userpass = md5('$password')");

    if(isset($user->userid)){

        $hash = bin2hex(random_bytes(27));

        $session = new stdClass();

        $session->userid = $user->userid;
        $session->userhash = $hash;
        $session->timemodified = time();

        $sessionID = $DB->insertRecord('bls_session', $session);

        if($sessionID){

            setcookie("login", $login, time() + 60 * 60 * 24 * 30);
            setcookie("password", $password, time() + 60 * 60 * 24 * 30);
            setcookie("sessionID", $sessionID, time() + 60 * 60 * 24 * 30);

            $_SESSION['hash'] = $hash;
            $_SESSION['userid'] = $user->userid;
            $_SESSION['login'] = $user->login;
            $_SESSION['position'] = $user->idposition;

        }

        header("Location: ../../personalArea.php");
        exit;

    } else {
        header("Location: ../../login.php");
    }
}

if($action == 'logout'){

    $DB->deleteRecordOnID('bls_session', $_COOKIE['sessionID']);
    session_unset();
    setcookie('login', '', time()-3600);
    setcookie('password', '', time()-3600);
    setcookie('sessionID', '', time()-3600);
    header("Location: ../../index.php");
    exit;
}


if(!isLogin()) {
    echo "<form method='post' action='login.php?action=login'>";
    echo "<input type='text' name='login'>";
    echo "<input type='text' name='userpass'>";
    echo "<input type='submit' name='submit' value='Войти'>";
    echo "</form>";
} else {
    echo "<h1>Hello, " . $_SESSION['username'] . "</h1>";
    echo "<form method='post' action='login.php?action=logout'>";
    echo "<input type='submit' name='submit' value='Выйти'>";
    echo "</form>";
}

//require_once ('/home/u607529909/domains/belils.ru/public_html/src/php/config.php');

//require_login();
//print_object($_SERVER['HTTP_USER_AGENT']);


//if($action == 'login'){
//
//    $user = $DB->getRecordSql("select userid, username from bls_user where login = '$login' and userpass = md5('$password')");
//
//    if(isset($user->userid)){
//
//        $_SESSION['userid'] = $user->userid;
//        $_SESSION['username'] = $user->username;
//        $_SESSION['login'] = $login;
//
//        $hash = bin2hex(random_bytes(27));
//
//        $session = new stdClass();
//
//        $session->userid = $user->userid;
//        $session->userhash = $hash;
//        $session->timemodified = time();
//
//        $sessionID = $DB->insertRecord('bls_session', $session);
//        if($sessionID) {
//            setcookie("hash", $hash, time() + 60 * 60 * 24 * 30);
//            setcookie("sessionID", $sessionID, time() + 60 * 60 * 24 * 30);
//            setcookie("userid", $user->userid, time() + 60 * 60 * 24 * 30);
//        }
//
//        header("Location: login.php");
//        exit;
//    } else {
//        echo "Данные введены неверно";
//    }
//
//}
//
//if($action == 'logout'){
//
//    $DB->deleteRecordOnID('bls_session', $_COOKIE['sessionID']);
//    session_unset();
//    setcookie('hash', '', time()-3600);
//    setcookie('sessionID', '', time()-3600);
//    header('Location: login.php');
//    exit;
//}