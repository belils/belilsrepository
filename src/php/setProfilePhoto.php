<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 01.10.2020
 * Time: 21:09
 */
require_once('config.php');

require_login();


$photoUser = $DB->getRecordSql("select f.id, f.file_path as filepath from bls_user u 
                                    inner join bls_files f on u.pictureid = f.id
                                    where userid = $USER->id");

if(isset($photoUser->id)){
    unlink($dirroot . $photoUser->filepath);
    $DB->deleteRecordOnID('bls_files', "id = $photoUser->id");
}


$path = null;

$path = upload_file('profilePhoto', "src/data/photo/$USER->id/");


if(isset($path)){
    $file = new stdClass();

    $file->user_id = $USER->id;
    $file->type = 'avatar';
    $file->file_path = $path;
    $file->timemodified = time();

    $fileID = $DB->insertRecord('bls_files', $file);

    unset($file);

    if($fileID){
        $rec = new stdClass();

        $rec->userid = $USER->id;
        $rec->pictureid = $fileID;

        $DB->updateRecord('bls_user', $rec);

        unset($rec);
        header('Location: ../../personalArea.php');
    }
}