<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 20.09.2020
 * Time: 15:50
 */



function has_capability($capability){
    global $DB;

//    require_login();
    if(isLogin()) {
        $id = $_SESSION['userid'];
        $capname = $DB->getRecordSql("select c.capname from bls_user u
            inner join bls_role_assignments ra on ra.userid = u.userid
            inner join bls_role r on r.role_id = ra.roleid
            inner join bls_role_capabilities rc on rc.role_id = r.role_id
            inner join bls_capabilities c on c.capname = rc.capability
            where u.userid = $id and capname = '$capability'");

        return isset($capname->capname);
    } else {
        return false;
    }
}

function require_login(){

    if (!isLogin()){
        header('Location: ../../login.php');
        exit;
    }
}


function isLogin(){
    global $DB;

    if (isset($_SESSION['hash'])){
        $sessionID = $DB->getRecordSql("select id from bls_session where userhash = '" . $_SESSION['hash'] . "'")->id;

        if ($sessionID){
            return true;
        } else {

            setcookie("login", '', time() - 3600);
            setcookie("password", '', time() - 3600);
            setcookie("sessionID", '', time() - 3600);
            session_unset();
            return false;
        }
    } else {
        return false;
    }
}

function isStudent(){
    global $DB, $USER;

    if (isset($_SESSION['position'])){
        return ($_SESSION['position'] <= 3)?true:false;
    } else {
        return ($DB->getRecordSql("select idposition from bls_user where userid = $USER->id")->idposition <= 3)?true:false;
    }
}


//function isLogin(){
//    global $DB;
//
//    if(isset($_COOKIE['hash']) && isset($_COOKIE['userid'])) {
//        $hash = $_COOKIE['hash'];
//        $userid = $_COOKIE['userid'];
//        $sessionID = $DB->getRecordSql("select id from bls_session where userid = '$userid' and userhash = '$hash'");
//
//        if ($sessionID){
//            return true;
//        } else {
//            setcookie('userid', '', time()-3600);
//            setcookie('hash', '', time()-3600);
//            setcookie('sessionID', '', time()-3600);
//            return false;
//        }
//
//    } else {
//        return false;
//    }
//
//}

function upload_file($fileName, $root){
    global $dirroot;

    $_FILES[$fileName]['name'] = str_replace('..', '.', transliterate($_FILES[$fileName]['name']));

    mkdir($dirroot. $root);

    $dir = $dirroot . $root . $_FILES[$fileName]["name"];

    if (move_uploaded_file($_FILES[$fileName]["tmp_name"], $dir)) {
        $fileUrl = $root . $_FILES[$fileName]["name"];
    } else {
        return null;
    }

    return $fileUrl;
}


function uploadMultiFile($fileName, $root){
    global $dirroot;

    $arrPath = [];

    foreach ($_FILES[$fileName]['name'] as $key => $file) {

        $name = str_replace('..', '.', transliterate($file));

        mkdir($dirroot . $root);

        $dir = $dirroot . $root . $name;

        if (move_uploaded_file($_FILES[$fileName]["tmp_name"][$key], $dir)) {
            $arrPath[] = $root . $name;
        } else {
            $arrPath[] = null;
        }
    }

    return $arrPath;
}


function file_download($fileName){

    if (file_exists($fileName)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($fileName).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($fileName));
        readfile($fileName);
        exit;
    }
}

function transliterate($input){

    $gost = array(
        "Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"-","є"=>"ye","ѓ"=>"g",
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
        "Е"=>"E","Ё"=>"YO","Ж"=>"ZH",
        "З"=>"Z","И"=>"I","Й"=>"J","К"=>"K","Л"=>"L",
        "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
        "С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X",
        "Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
        "Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
        "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
        "е"=>"e","ё"=>"yo","ж"=>"zh",
        "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x",
        "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
        "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=>"_","—"=>"_",","=>"_","!"=>"_","@"=>"_",
        "#"=>"-","$"=>"","%"=>"","^"=>"","&"=>"","*"=>"",
        "("=>"",")"=>"","+"=>"","="=>"",";"=>"",":"=>"",
        "'"=>"","\""=>"","~"=>"","`"=>"","?"=>"","/"=>"",
        "\\"=>"","["=>"","]"=>"","{"=>"","}"=>"","|"=>"" ,"."=>"."
    );

    return strtr($input, $gost);
}

function print_object($value){
    echo "<pre style='background: #c5c5c5'>";
    print_r($value);
    echo "</pre>";
}

function printFileDir($path = './'){

    if($handle = opendir($path)){
        while($entry = readdir($handle)){
            echo $entry."<br>";
        }

        closedir($handle);
    }
}

function createLinkFile($path, $text = 'Скачать'){
    return "<a href = '/src/php/download.php?filePath=$path'>$text</a>";
}

function data_submitted() {

    if (empty($_POST)) {
        return false;
    } else {
        return (object) $_POST;
    }
}

function optional_param($parname, $default) {

    if (isset($_POST[$parname])) {
        $param = $_POST[$parname];
    } else if (isset($_GET[$parname])) {
        $param = $_GET[$parname];
    } else {
        return $default;
    }

    return $param;
}



//function returnArhiveSelect(){

//    $yearStart = 2015;
//    $yearEnd = date('Y', time());

//    $result = "<select name='town' id='s1'>";
//    $result .= "<option value='0'>АРХИВ</option>";

//    $result='';
//    $result .= "<div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>";
//
//    for($i = $yearStart; $i<=$yearEnd; $i++){
////        $result .="<div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>";
////        $result .= "<option value='$i' >$i</option>";
//
//
//        $result .= "<a class='dropdown-item sss' href='/archive.php?year=".$i."'>$i</a>";
//
//    }
//    $result .= "</div>";
//    return $result;
//        . "</select>";
//}