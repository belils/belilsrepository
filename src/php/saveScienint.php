<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 04.10.2020
 * Time: 17:27
 */
require_once('config.php');

$frm = data_submitted();


if($frm->typeBtn == 'del'){
    $DB->deleteRecordOnID('bls_scientific_interests', "id = $frm->id");
    echo json_encode(['status' => 'deleted']);
}

if($frm->typeBtn == 'add'){
    $scienint = new stdClass();

    $scienint->text = $frm->textValue;
    $scienint->unused = ($frm->checked=='true')?1:0;

    if($id = $DB->insertRecord('bls_scientific_interests', $scienint)){
        echo json_encode([
            'status' => 'added',
            'id' => $id
        ]);
    } else {
        echo json_encode(['status' => 'error']);
    }
}

if($frm->typeBtn == 'sav'){
    $scienint = new stdClass();

    $scienint->id = $frm->id;
    $scienint->text = $frm->textValue;
    $scienint->unused = ($frm->checked=='true')?1:0;

    if($DB->updateRecord('bls_scientific_interests', $scienint) !== false){
        echo json_encode(['status' => 'updated']);
    } else {
        echo json_encode(['status' => 'error']);
    }
}

//if(has_capability('adminpanel:view')){
//
//    $scienint = new stdClass();
//
//    $scienint->id = '';
//    $scienint->text = '';
//    $scienint->unused = '';
//
//    if($DB->updateRecord('bls_scientific_interests', $scienint) !== false){
//        echo json_encode(['status' => 'ok']);
//    } else {
//        echo json_encode(['status' => 'error']);
//    }
//} else {
//    echo json_encode(['status' => 'notaccess']);
//}
