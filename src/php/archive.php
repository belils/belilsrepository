<?php
require_once ('config.php');

$year = $_GET['year'];

$sql = "select t.thesis_id, u.username,from_unixtime(date_app, '%d.%m.%Y') as dateapp, 
                        tt.thesistext, f.file_path
                        from bls_thesis t
                        inner join bls_thesis_type tt on tt.id_thesis_type = t.id_thesis_type
                        left join bls_thesis_info ti on ti.id_thesis = t.thesis_id
                        inner join bls_files f on f.id = t.file_id
                        inner join bls_user u on t.user_id = u.userid 
                        where from_unixtime(date_app,'%Y') = $year";

$list = $DB->getRecordsSql($sql);

echo json_encode(['list'=>$list]);
?>