<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 10.10.2020
 * Time: 19:59
 */
require_once('config.php');

$dataType = optional_param('datatype', null);

if($dataType == 'userList'){
    $userList = $DB->getRecordsSql("select u.userid, concat(u.surname, ' ', u.username) as username,
                                u.birthday, u.country, u.city, u.birthday, p.text as position,
                                u.degree, u.email, u.phone
                                from bls_user u
                                inner join bls_position p on u.idposition = p.id
                                where u.userid <> 1");

    echo json_encode($userList);
}

if($dataType == 'userThesisList'){
    $userList = $DB->getRecordsSql("select u.userid, concat(u.surname, ' ', u.username) as username,
                                u.birthday, u.country, u.city, u.birthday, p.text as position,
                                u.degree, u.email, u.phone
                                from bls_user u
                                inner join bls_position p on u.idposition = p.id
                                where u.userid <> 1");
    $resultArr = [];
    foreach ($userList as $key => $user){
        $thesisList = $DB->getRecordsSql("select t.thesis_id, t.user_id, from_unixtime(date_app, '%d.%m.%Y') as dateapp,
                        si.text as scientext, tt.thesistext,
                        from_unixtime(ti.arrival_date, '%d.%m.%Y') as arrival_date,
                        ti.transfer, ti.coffee, ti.lunch, ti.dinner, f.file_path
                        from bls_thesis t
                        inner join bls_thesis_type tt on t.id_thesis_type = tt.id_thesis_type
                        left join bls_thesis_info ti on t.thesis_id = ti.id_thesis
                        inner join bls_files f on t.file_id = f.id
                        inner join bls_scientific_interests si on t.id_scienint = si.id
                        where t.user_id = " . $user->userid);

        if(count($thesisList) > 0){
            $userList[$key]->thesisList = $thesisList;
            $resultArr [] = $userList[$key];
        } /*else {
            unset($userList[$key]);
        }*/
    }

    echo json_encode($resultArr);
}


if($dataType == 'userDocList'){
    $userList = $DB->getRecordsSql("select u.userid, concat(u.surname, ' ', u.username) as username,
                                u.birthday, u.country, u.city, u.birthday, p.text as position,
                                u.degree, u.email, u.phone
                                from bls_user u
                                inner join bls_position p on u.idposition = p.id
                                where u.userid <> 1");
    $resultArr = [];
    foreach ($userList as $key => $user) {
        $skillList = $DB->getRecordsSql("select s.id, s.userid, from_unixtime(s.timemodified, '%d.%m.%Y') as timemodified,
                                        f.file_path
                                        from bls_skill s
                                        inner join bls_files f on s.fileid = f.id
                                        where s.userid = " . $user->userid);

        if(count($skillList) > 0){
            $userList[$key]->docList = $skillList;
            $resultArr [] = $userList[$key];
        } /*else {
            unset($userList[$key]);
        }*/
    }

    echo json_encode($resultArr);
}