<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 25.10.2020
 * Time: 17:58
 */
require_once('config.php');


$userID = optional_param('userid', null);

if(!is_null($userID)){

    $fileList = $DB->getRecordsSql("select s.id, f.file_path from bls_skill s
                                inner join bls_files f on s.fileid = f.id
                                where userid = $userID");
//    print_object($fileList); exit;

    $zip = new ZipArchive();
    $filename = 'data.zip';

    if($zip->open($filename, ZipArchive::CREATE) !== true){
        // print_object('error');
        exit;
    }


    foreach($fileList as $file){
        $nameFile = end(explode('/', $file->file_path));
        $zip->addFile($dirroot . $file->file_path, $nameFile);
    }

    $zip->close();

    if (file_exists($filename)) {
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename="'.basename($filename).'"');
        readfile($filename);
    }

    unlink($filename);
}