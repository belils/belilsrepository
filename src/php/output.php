<?php 
class Output {

	private $header;
	private $footer;
	private $title = "Belils";
	private $link = [" <link rel='shortcut icon' href='src/data/img/logo-belgu.png' type='image/x-icon'>"];
	private $meta = [];
//	private $dirPath = '';

    private $menuList = [
        // ['name' => 'Партнеры', 'link' => '#'],
        ['name' => 'О проекте', 'link' => 'index.php'],
        ['name' => 'Личный кабинет', 'link' => 'personalArea.php'],
        ['name' => 'Орг. комитет', 'link' => 'orgcomm.php'],
        ['name' => 'Контакты', 'link' => 'contact.php']
    ];

    private $menuListEN = [
        // ['name' => 'Партнеры', 'link' => '#'],
        ['name' => 'About the project', 'link' => 'index.php'],
        ['name' => 'Personal area', 'link' => 'personalArea.php'],
        ['name' => 'Org. committee', 'link' => 'orgcomm.php'],
        ['name' => 'Contacts', 'link' => 'contact.php']
    ];


    function __construct(){

		$str = "<meta charset='UTF-8'/>";
		array_push($this->meta, $str);

		$str = "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
		array_push($this->meta, $str);

//        $str = "<link rel='stylesheet' href='./src/css/style.css'>";
//        $str = "<link rel='stylesheet' href='./src/template/29/style.css'>";
//		$str = "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'>";
        $str = "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">";
		array_push($this->link, $str);

        $str = "<link rel='stylesheet' href='./src/css/style2.css'>";
        array_push($this->meta, $str);

		unset($str);
	}

	public function setHeader($header){
		$this->header = $header;
	}

	 public function setFooter($footer){
	 	$this->footer = $footer;
	}

	public function header(){
		$result = "<!DOCTYPE html>
						<html>
						<head>";
		foreach ($this->meta as $value) {
			$result .= $value;
		}
		$result .= '<title>' . $this->title . '</title>';
		foreach ($this->link as $value) {
			$result .= $value;
		}
		$result .= "</head>
					<body>";


       $path = ($_COOKIE['lang']=='en')?'src/template/en/header.html':'src/template/29/header.html';
//        $path = "./src/template/29/header.html";

        if(file_exists($path)){
            $result .= "<div class='wrapper'>
                        <div class='content'>";

            $headerTemplate = file_get_contents($path);


            if(has_capability('adminpanel:view')){
                $this->menuList[] = ['name' => 'Админ панель', 'link' => 'admin.php'];
                $this->menuListEN[] = ['name' => 'Admin panel', 'link' => 'admin.php'];

            }

            $elementList = $_COOKIE['lang'] == 'en'?$this->menuListEN:$this->menuList;
            $menu = '';
            foreach($elementList as $element){

//                $menu .= "<li><a class='' href='" . $element['link'] . "'>" . $element['name'] . "</a></li>";
                $menu .= "<li class='nav-item'><a class='nav-link' href='" . $element['link'] . "'>" . $element['name'] . "</a></li>";
            }

            $headerTemplate = str_replace('#menu#', $menu, $headerTemplate);

            // Вход/Выход
            $login = '';
            if (isLogin()){
                $login .= "<li class='nav-item'><a class='nav-link' href='src/php/login.php?action=logout'>" . ($_COOKIE['lang'] == 'en'?'Logout':'Выйти') . "</a></li>";
            } else {
                $login .= "<li class='nav-item'><a class='nav-link' href='login.php'>" . ($_COOKIE['lang'] == 'en'?'Login':'Войти') . "</a></li>";
            }

            $headerTemplate = str_replace('#login#', $login, $headerTemplate);

            // Вывод архива в шапку
            $archive = '';
            for($i = 2020; $i <= date('Y', time()); $i++){
                $archive .= "<a class='dropdown-item' href='archive.php?year=$i'>$i</a>";
            }

            $headerTemplate = str_replace('#archive#', $archive, $headerTemplate);

            $username = isLogin()?$_SESSION['login']:'';

            $headerTemplate = str_replace('#username#', $username, $headerTemplate);

            if ($_COOKIE['lang'] == 'en'){
                $langText = "<a class='nav-link' href = 'src/php/lang.php?lang=ru'>RU</a>";
            } else {
                $langText = "<a class='nav-link' href = 'src/php/lang.php?lang=en'>EN</a>";
            }

            $headerTemplate = str_replace('#lang#', $langText, $headerTemplate);

            $result .= $headerTemplate;
        }

		return $result;
	}

	public function footer(){


//        $templatePath = ($_COOKIE['lang']=='en')?'src/template/en/index.html':'src/template/29/index.html';


	    $path = "./src/template/29/footer.html";
	    $path = ($_COOKIE['lang']=='en')?'src/template/en/footer.html':'src/template/29/footer.html';
	    $result = '';

	    if(file_exists($path)){
            $result .= file_get_contents($path);
            $result .= "</div></div>";
        }

		$result .= "</body>
					</html>";

		return $result;
	}

	public function addLink($rel, $path){
        $str = "<link rel='$rel' src='$path'>";
        array_push($this->meta, $str);
	}

	public function setTitle($title){
		$this->title = $title;
	} 

	public function emptyTag($tagName, $attributes){
		$result = '<' . $tagName . ' ';
		foreach ($attributes as $key => $value) {
			$result .= $key . "='" . $value . "' ";
		}
		return $result . '/>';
	}
}