<?php
require_once('config.php');

$zip = new ZipArchive();
$filename = 'Archive2020.zip';


if($zip->open($filename, ZipArchive::CREATE) !== true){
    // print_object('error');
    exit;
}

$listFiles = $DB->getRecordsSql("SELECT DISTINCT t.user_id, si.text, 
							concat(u.surname, ' ', u.username) as username,
							f.file_path
							FROM bls_thesis t
							inner join bls_user u on t.user_id = u.userid
							inner join bls_scientific_interests si on t.id_scienint = si.id
							inner join bls_skill s on t.user_id = s.userid
							inner join bls_files f on s.fileid = f.id");

$size = 0;
foreach($listFiles as $key => $file){
	print_object(filesize($dirroot . $file->file_path) / (1000 * 1000));
	$size = $size + filesize($dirroot . $file->file_path);
}

print_object('SIZE: ' . $size); exit;


foreach($listFiles as $file){

    $nameFile = end(explode('/', $file->file_path));
    $pathThesis = $file->text . '/' . $file->username . '/' . $nameFile;
    // print_object($dirroot . $file->file_path);
    // print_object($file->file_path);
   	// var_dump(file_exists($dirroot . $file->file_path));
    $zip->addFile($dirroot . $file->file_path, $pathThesis);
}

$zip->close();

if (file_exists($filename)) {
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    readfile($filename);
}

unlink($filename);