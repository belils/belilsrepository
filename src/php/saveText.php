<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 04.10.2020
 * Time: 15:31
 */
require_once('config.php');

$frm = data_submitted();

if(has_capability('adminpanel:view')){

    $caption = new stdClass();

    $caption->type = $frm->data;
    $caption->text = $frm->text;

    if($DB->updateRecord('bls_captions', $caption) !== false){
        echo json_encode(['status' => 'ok']);
    } else {
        echo json_encode(['status' => 'error']);
    }
} else {
    echo json_encode(['status' => 'notaccess']);
}