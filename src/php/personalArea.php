<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 27.09.2020
 * Time: 14:40
 */

require_once('config.php');

require_login();

$frm = data_submitted();

print_object($frm);

$record = new stdClass();

$record->userid = $frm->userid;
$record->username = $frm->username;
$record->surname = $frm->surname;
$record->birthday = $frm->birthday;
$record->city = $frm->city;
$record->organization = $frm->organization;
$record->idposition = $frm->position;
$record->degree = $frm->power;
$record->email = $frm->email;
$record->phone = $frm->number;

$DB->updateRecord('bls_user', $record);

header('Location: ../../personalArea.php');