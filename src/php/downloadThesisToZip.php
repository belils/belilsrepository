<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 23.10.2020
 * Time: 20:35
 */
require_once('config.php');

//$th = $DB->getRecordsSql("select thesis_id, file_id from bls_thesis where user_id <> 1");
//
//$count = 0;
//$record = new stdClass();
//foreach ($th as $item){
//
//    $record->thesisid = $item->thesis_id;
//    $record->fileid = $item->file_id;
//
//    $DB->insertRecord('bls_thesis_file', $record);
//    $count++;
//
//}
//unset($record);
//
//print_object("INSERTED: $count");
//
//
//exit;

$thesisID = optional_param('thesisid', null);

if(!is_null($thesisID)){

    $fileList = $DB->getRecordsSql("select tf.id, f.file_path from bls_thesis_file tf
                                        inner join bls_files f on tf.fileid = f.id
                                        where tf.thesisid = $thesisID");

    $zip = new ZipArchive();
    $filename = 'data.zip';

    if($zip->open($filename, ZipArchive::CREATE) !== true){
        // print_object('error');
        exit;
    }


    foreach($fileList as $file){
        $nameFile = end(explode('/', $file->file_path));
        $zip->addFile($dirroot . $file->file_path, $nameFile);
    }

    $zip->close();

    if (file_exists($filename)) {
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename="'.basename($filename).'"');
        readfile($filename);
    }

    unlink($filename);
}