<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 25.10.2020
 * Time: 19:04
 */

require_once('config.php');

$lang = optional_param('lang', 'ru');

setcookie("lang", $lang, time() + 60 * 60 * 24 * 30, '/');

header("Location: ../../index.php");