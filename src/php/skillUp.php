<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 03.10.2020
 * Time: 17:49
 */
require_once('config.php');

require_login();

$path = null;

$filePathArr = uploadMultiFile('fileSkill', "src/data/qualif/$USER->id/");



for ($i=0; $i<count($filePathArr); $i++) {

    if (isset($filePathArr)) {
        $file = new stdClass();

        $file->user_id = $USER->id;
        $file->type = 'qualification';
        $file->file_path = $filePathArr[$i];
        $file->timemodified = time();

        $fileID = $DB->insertRecord('bls_files', $file);

        unset($file);

        if ($fileID) {
            $rec = new stdClass();

            $rec->userid = $USER->id;
            $rec->fileid = $fileID;
            $rec->timemodified = time();

            $DB->insertRecord('bls_skill', $rec);

            unset($rec);
        }
    }
}

header('Location: ../../personalArea.php');