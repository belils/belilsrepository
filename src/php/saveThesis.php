<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 01.10.2020
 * Time: 23:20
 */


require_once ('config.php');
require_login();

//print_object("OK");
$frm = data_submitted();

// uploadMultiFile
//$path = upload_file('fileThesis',"src/data/thesis/$USER->id/");
$path = uploadMultiFile('fileThesis',"src/data/thesis/$USER->id/");
$fileID = [];
if ($path){
    foreach($path as $onePath) {
        $record = new stdClass();

        $record->user_id = $USER->id;
        $record->type = 'thesisFile';
        $record->file_path = $onePath;
        $record->timemodified = time();

        $fileID [] = $DB->insertRecord('bls_files', $record);
        unset($record);
    }

    if ($fileID[0]){
        //ничего не трогать
        $record = new stdClass();

        $record->id_thesis_type = $frm->thesisType;
        $record->date_app = time();
//        $record->file_id = $fileID;
        $record->user_id = $USER->id;
        $record->id_scienint = $frm->scienint;
        $record->id_thesis_form = $frm->thesisForm;

//        print_object($record);

        $thesisID = $DB->insertRecord('bls_thesis', $record);
        unset($record);

        foreach($fileID as $oneFileId) {
            $record = new stdClass();

            $record->thesisid = $thesisID;
            $record->fileid = $oneFileId;

            $DB->insertRecord('bls_thesis_file', $record);
            unset($record);
        }

        //не трогаем
        if($frm->thesisType == 1){
            $record = new stdClass();

            $record->id_thesis = $thesisID;
            $record->arrival_date = strtotime($frm->data1);
            $record->transfer = $frm->r1;
            $record->coffee = isset($frm->checkbox1)?1:0;
            $record->lunch = isset($frm->checkbox2)?1:0;
            $record->dinner = isset($frm->checkbox3)?1:0;

//            print_object($record);

            $DB->insertRecord('bls_thesis_info', $record);
        }
    }

}


header('Location: ../../personalArea.php');



