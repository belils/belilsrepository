<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 02.10.2020
 * Time: 21:36
 */
require_once('config.php');

require_login();

$thesisID = optional_param('thesisid', null);

$thesis = $DB->getRecordsSql("select t.thesis_id, f.id as fileid, f.file_path as filepath,
                                ti.id as thesisinfoid, tf.id as thesisfileid
                                from bls_thesis t
                                inner join bls_thesis_file tf on t.thesis_id = tf.thesisid
                                inner join bls_files f on f.id = tf.fileid
                                left join bls_thesis_info ti on ti.id_thesis=t.thesis_id
                                where t.thesis_id = $thesisID and  t.user_id = $USER->id");

//print_object("select t.thesis_id, f.id as fileid, f.file_path as filepath,
//                                ti.id as thesisinfoid
//                                from bls_thesis t
//                                inner join bls_files f on f.id = t.file_id
//                                left join bls_thesis_info ti on ti.id_thesis=t.thesis_id
//                                where t.thesis_id = $thesisiD and  t.user_id = $USER->id"); exit;

if(isset($thesis[0]->filepath)){
    foreach($thesis as $oneThesis) {
        $DB->deleteRecordOnID('bls_files', "id = $oneThesis->fileid");
        unlink($dirroot . $oneThesis->filepath);
        $DB->deleteRecordOnID('bls_thesis_file', "id = " . $oneThesis->thesisfileid);
    }

    if (!is_null($oneThesis->thesisinfoid)) {
        $DB->deleteRecordOnID('bls_thesis_info', "id = " . $thesis[0]->thesisinfoid);
    }
    $DB->deleteRecordOnID('bls_thesis', "thesis_id = " . $thesis[0]->thesis_id);

    header('Location: ../../personalArea.php');
}

header('Location: ../../personalArea.php');
