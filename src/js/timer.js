function tick(timerName) {
    let day = document.querySelector('#day')
    let hour = document.querySelector('#hour')
    let minute = document.querySelector('#minute')
    let second = document.querySelector('#second')

    let dd = day.innerText
    let hh = hour.innerText
    let mm = minute.innerText
    let ss = second.innerText

    decSecond(timerName)

}

function decSecond(timerName){

    let secondInMinute = 60
    let second = document.querySelector('#second' + timerName)
    let ss = second.innerText

    ss--
    if(ss > 0){
        second.innerText = formatTime(ss)
    } else {
        second.innerText = secondInMinute -1
        decMinute(timerName)
    }
}

function decMinute(timerName){

    let minuteInHour = 60;
    let minute = document.querySelector('#minute' + timerName)
    let mm = minute.innerText

    mm--
    if(mm > 0){
        minute.innerText = formatTime(mm)
    } else {
        minute.innerText = minuteInHour - 1
        decHour(timerName)
    }
}

function decHour(timerName){

    let hourInDay = 24;
    let hour = document.querySelector('#hour' + timerName)
    let hh = hour.innerText

    hh--
    if(hh > 0){
        hour.innerText = formatTime(hh)
    } else {
        hour.innerText = hourInDay - 1
        decDay(timerName)
    }

}

function decDay(timerName){

    let dayInYear = 365;
    let day = document.querySelector('#day' + timerName)
    let dd = day.innerText

    dd--
    if(dd > 0){
        day.innerText = dd
    } else {
        day.innerText = dayInYear - 1
    }

}

function setTimer(timerName){

    let mounthEnd = 10;
    let dayEnd = 30;
    let hourEnd = 23;
    let minuteEnd = 59;
    let secontEnd = 59;
    let msecondEnd = 0;

    let day = document.querySelector('#day' + timerName)
    let hour = document.querySelector('#hour' + timerName)
    let minute = document.querySelector('#minute' + timerName)
    let second = document.querySelector('#second' + timerName)

    dt = new Date()
    dt2 = new Date()

    dt2.setMonth(mounthEnd - 1)
    dt2.setDate(dayEnd)
    dt2.setHours(hourEnd)
    dt2.setMinutes(minuteEnd)
    dt2.setSeconds(secontEnd)
    dt2.setMilliseconds(msecondEnd)


    ms = dt2 - dt

    md = Math.floor(ms / 1000 / 60 / 60 / 24)
    day.innerText = md

    ms = ms % (1000 * 60 * 60 * 24)

    mh = Math.floor(ms / 1000 / 60 / 60 )

    hour.innerText = formatTime(mh)

    ms = ms % (1000 * 60 * 60)

    mmin = Math.floor(ms / 1000 / 60 )
    minute.innerText = formatTime(mmin)

    ms = ms % (1000 * 60)

    ms = Math.floor(ms / 1000 )
    second.innerText = formatTime(ms)
}

function formatTime(time){
    if(time < 10){
        return '0' + time
    } else {
        return time
    }
}

setTimer('')
setTimer('_s')
timer = setInterval(tick, 1000, [''])
timer = setInterval(tick, 1000, ['_s'])
