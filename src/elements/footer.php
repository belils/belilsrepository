    <!-- ////////SCRIPTS//////// -->
    

    <script>
        var btn = document.querySelector('.btn1');
        var modal = document.querySelector('.modal');
        var close = document.querySelector('.close');



        btn.onclick = function () {
            modal.style.display = 'block';
        }

        close.onclick = function () {
            modal.style.display = 'none';
        }

        window.onclick = function (e) {
            if (e.target == modal) {
                modal.style.display = 'none';
            }
        }

    </script>

<footer class="footer">
    <div class="container">
        <div class="footer__body">
            <div class="footer__left">
                <div class="cont">
                    <h3>Контакты</h3>
                </div>
                <div class="cont_cont">
                    <p>Адрес: Победы 85А</p>
                    <p>Телефон: 8-800-555-35-35</p>
                    <p>E-mail: belils@mail.com</p>
                </div>
            </div>
            <div class="footer__right">
                <div class="qr">
                    <h3>Отсканируйте QR-код</h3>
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/QR_code_for_mobile_English_Wikipedia.svg/768px-QR_code_for_mobile_English_Wikipedia.svg.png"
                        alt="">
                </div>
            </div>
        </div>
    </div>
</footer>



</body>

</html>