        <!-- <button class="btn">Open Modal</button> -->
        <div class="modal">
            <div class="modal_content">

                <button class="close">X</button>



                <link rel="stylesheet" href="https://bootstraptema.ru/plugins/2015/bootstrap3/bootstrap.min.css" />
                <script src="https://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>
                <script src="https://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>
                <script src="https://bootstraptema.ru/plugins/2016/validator/validator.min.js"></script>


                <!-- <div class="container"> -->
                <!-- <div class="row"> -->
                <div class="col-md-10 col-sm-offset-1">


                    <form data-toggle="validator" role="form">

                        <div class="form-group">
                            <h3>Регистрация</h3>
                            <label for="inputName" class="control-label">Фамилия</label>
                            <input type="text" class="form-control" id="surname" placeholder="Введите вашу фамилию"
                                required>
                        </div>

                        <div class="form-group">
                            <label for="inputName" class="control-label">Имя</label>
                            <input type="text" class="form-control" id="name" placeholder="Введите Ваше имя"
                                required>
                        </div>


                        <div class="form-group ">
                            <label for="example-date-input" class=" col-form-label">Дата рождения</label>
                            <div class="">
                                <input class="form-control" type="date" value="2011-08-19"
                                    id="brth">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputName" class="control-label">Город</label>
                            <input type="text" class="form-control" id="city" placeholder="Введите город"
                                required>
                        </div>

                        
                        <div class="form-group">
                            <label for="inputName" class="control-label">Организация</label>
                            <input type="text" class="form-control" id="organization" placeholder="Введите организацию"
                                required>
                        </div>


                        <div class="form-group">
                            <label for="inputName" class="control-label">Должность</label>
                            <input type="text" class="form-control" id="position" placeholder="Введите должность"
                                required>
                        </div>

                        <div class="form-group">
                            <label for="inputName" class="control-label">Область научных интересова</label>
                            <input type="text" class="form-control" id="hobby" placeholder="Введите область научных интересов"
                                required>
                        </div>


                        <div class="form-group">
                            <label for="inputName" class="control-label">Ученая степень</label>
                            <input type="text" class="form-control" id="power" placeholder="Введите ученную степень"
                                required>
                        </div>
                        

                        <div class="form-group">
                            <label for="inputEmail" class="control-label">Ваш E-mail</label>
                            <input type="email" class="form-control" id="email" placeholder="Email"
                                data-error="Вы не правильно ввели Ваш E-mail" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail" class="control-label">Контактный телефон</label>
                            <input type="number" class="form-control" id="number" placeholder="8-800-555-35-35"
                                data-error="Вы ввели неправльный телефон" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        
                        <div class="form-group">
                            <label for="inputName" class="control-label">Логин</label>
                            <input type="text" class="form-control" id="login" placeholder="Введите логин"
                                required>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword" class="control-label">Введите пароль</label>
                            <div class="form-inline row">
                                <div class="form-group col-sm-7">
                                    <input type="password" data-toggle="validator" data-minlength="2"
                                        class="form-control" id="password" placeholder="123456" required>
                                    <span class="help-block">Минимум 6 значений</span>
                                </div>
                                <div class="form-group col-sm-6">
                                    <input type="password2" class="form-control" id="password2"
                                        data-match="#password" data-match-error="Ошибка! Пароли не совпадают!"
                                        placeholder="Повторите пароль" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        
 
                       
<!--                        <div class="form-group">-->
<!--                            <div class="checkbox">-->
<!--                                <label>-->
<!--                                    <input type="checkbox" id="terms" data-error="Прежде чем оправить данные" required>-->
<!--                                    Докажите что Вы человек-->
<!--                                </label>-->
<!--                                <div class="help-block with-errors"></div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="form-group ">
                            <button type="button" class="btn btn-outline-primary col-md-10 col-sm-offset-1" id="btn10">Отправить</button>
                        </div>

              
                          
                        
                    </form>


                </div>
                <!-- </div> -->
                <!-- </div> -->


            </div>
        </div>

        <script>
            var btn = document.querySelector('.btn1');
            var modal = document.querySelector('.modal');
            var close = document.querySelector('.close');


 
            btn.onclick = function () {
                modal.style.display = 'block';
            }

            close.onclick = function () {
                modal.style.display = 'none';
            }

            window.onclick = function (e) {
                if (e.target == modal) {
                    modal.style.display = 'none';
                }
            }

        </script>