<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 07.10.2020
 * Time: 2:44
 */
require_once("src/php/config.php");


$OUTPUT->setTitle('Эксперты');

echo $OUTPUT->header();

$orgCard = file_get_contents('src/template/29/orgCard.html');

$content = '';
$search = [
    '#src#',
    '#text#'
];

$content .= '<hr>';
foreach (getListExp() as $key => $value) {

    $path = "src/data/expert/" . $key . ".jpg";
    if (file_exists($path)) {
        $content .= str_replace($search, [$path, $value], $orgCard);
        $content .= '<hr>';
    }
}

echo "<section class=\"content__org\">
      <div class=\"container\">";


echo "<h1 style='text-align: center'>Наши эксперты</h1>";
echo $content;

echo "</div></section>";


echo $OUTPUT->footer();




function getListExp(){
    $list = [
        1 => "<h3>Донник Ирина Михайловна</h3> Вице-президент РАН, академик РАН, Руководитель НПП 3 НОЦ \"Селекционно-генетические исследования, клеточные технологии и генная инженерия: животноводство\"",
        2 => "<h3>Кирпичников Михаил Петрович</h3> российский учёный в области физико-химической биологии, белковой инженерии и биотехнологии; государственный и общественный деятель декан биологического факультета МГУ им. М.В. Ломоносова, академик РАН",
        3 => "<h3>Галстян Арам Генрихович</h3>Врио директора ФГАНУ «ВНИМИ», Академик РАН, руководитель НПП 4 НОЦ \"Здровьесберегающие технологии: производство продовольствия и ветпрепаратов\"",
        4 => "<h3>Кульчин Юрий Николаевич</h3> д.физ.н., директор Института автоматики и процессов управления ДВО РАН академик РАН",
        5 => "<h3>Надыкта Владимир Дмитриевич</h3>Главный научный консультант, ФГНУ ВНИИ БЗР, академик РАН (Краснодар)",
        6 => "<h3>Синеговская Валентина Тимофеевна</h3> д. с-х.н., Председатель ОУС ДВО РАН, главный научный сотрудник ФГБНУ «Всероссийский научно-исследовательский институт сои», академик РАН",
        7 => "<h3>Карлов Геннадий Ильич</h3> директор ФГБНУ «Всероссийский научно-исследовательский институт сельскохозяйственной биотехнологии», академик РАН",
        8 => "<h3>Терентьев Александр Олегович</h3> российский химик-технолог, специалист в области органической и технической химии, член-корреспондент РАН, зам. директора Института Органической химии им. Н.Д. Зелинского РАН",
        9 => "<h3>Глинушкин Алексей Павлович</h3> российский фитопатолог, член-корреспондент РАН, директор ФГБНУ ВНИИФ",
        10 => "<h3>Горбунова Юлия Германовна</h3> главный научный сотрудник ИОНХ РАН и ИФХЭ РАН, доктор химических наук, профессор, профессор РАН, член-корреспондент РАН",
        11 => "<h3>Журавлева Екатерина Васильевна</h3> заместитель начальника департамента-начальник управления науки департамента внутренней и кадровой политики Белгородской области, руководитель НПП 2 НОЦ \"Селекционно-генетические исследования, клеточные технологии и генная инженерия (в области растениеводства)\", профессор РАН",
        12 => "<h3>Шилов Илья Александрович Заведующий</h3> доктор биологических наук, профессор РАН",
        13 => "<h3>Бабушкин Вадим Анатольевич</h3>  ректор Мичуринского государственного аграрного университета, доктор сельскохозяйственных наук",
        14 => "<h3>Упелниек Владимир Петрович</h3> к.б.н., директор ФГБНУ «Главный ботанический сад им. Н.В. Цицина» (г. Москва)",
        15 => "<h3>Надточенко Виктор Андреевич</h3> ученый-химик, доктор химических наук, профессор, лауреат премии имени Ю. А. Овчинникова (2012), директор ФГБУН «Федеральный исследовательский центр химической физики им. Н.Н. Семенова» РАН",
        16 => "<h3>Воронов Сергей Иванович</h3> д.б.н.,  директор ФГБНУ «Федеральный исследовательский центр «Немчиновка», руководитель НПП 5 НОЦ, \"Рациональное приропользование\"",
        17 => "<h3>Чесноков Юрий Валентинович</h3> д.б.н., директор ФГБНУ «Агрофизический институт», (г. Санкт-Петербург)",
        18 => "<h3>Кощаев Андрей Георгиевич</h3> проректор по научной работе ФГБОУ ВО «Кубанский государственный аграрный университет имени И. Т. Трубилина»",
        19 => "<h3>Константинов Игорь Сергеевич</h3> проректор по науке и инновационному развитию ФГБОУ ВО РГАУ-МСХА",
        20 => "<h3>Будаговский Андрей Валентинович</h3> д.т.н., заведующий лабораторией «Биофотоника» ФГБОУ ВО Мичуринский ГАУ",
        21 => "<h3>Кочиева Елена Зауровна</h3> д.б.н., профессор, МГУ имени М. В. Ломоносова, Биологический факультет, Кафедра биотехнологии",
        22 => "<h3>Давыдова Наталья Владимировна</h3> д. с-х.н., заведующая лабораторией ФГБНУ «Федеральный исследовательский центр Немчиновка»",
        23 => "<h3>Цедилин Андрей Николаевич</h3> к.т.н., доцент кафедры «Процессы и аппараты химической технологии» Московского политехнического университета",
        24 => "<h3>Молканова Ольга Ивановна</h3> к.с-х.н., заведующая отделом ФГБНУ «Главный ботанический сад им. Н.В. Цицина» (г. Москва)",
        25 => "<h3>Джихуа Ван</h3>директор Шаньдунской ключевой лаборатории биофизики Университета Дэчжоу (Китай)",
        26 => "<h3>Кистаубаева Аида Сериковна</h3> заведующий кафедрой биотехнологии, Казахский национальный университет им. аль-Фараби, кандидат биологических наук, доцент (Казахстан)",
        27 => "<h3>Ёзиев Лутфулло Хабибуллаевич</h3> зав. кафедрой ботаники и экологии, Каршинский государственный университет, доктор биологических наук, профессор (Узбекистан)",
        28 => "<h3>Нарзулло Бобоевич Саидов</h3> декан медико-фармацевтического факультета Национального университета Таджикистана, доктор фармацевтических наук (Таджикистан)",
        29 => "<h3>Петер Лангер</h3> заведующий отделом органического синтеза Института катализа им. Лейбница, Ростокский университет (Германия)",
        30 => "<h3>Петр Солих</h3>руководитель аналитического департамента фармацевтического факультета Карлова университета (Чехия)",
        31 => "<h3>Роб ван Харен</h3> профессор Университета прикладных наук Ханзе (Нидерланды)",
        32 => "<h3>Чечилия Джеорджеску</h3> доцент факультета сельскохозяйственных наук, пищевой промышленности и охраны окружающей среды Университета Лучиан Блага Сибиу, (Румыния)",
        33 => "<h3>Шридхар Нараянан</h3> главный научный сотрудник лаборатории Шриниваса Гуллапалли, PhD (Индия)"

    ];

    return $list;
}