<?php

require_once('src/php/config.php');

require_login();

$OUTPUT->setTitle('Личный кабинет изменения');
echo $OUTPUT->header();

$personalArea = file_get_contents("src/template/personalAreaEdit.html");



$personalData = $DB->getRecordSql("select userid, concat(surname, ' ', username) as username from bls_user where userid = " . $_SESSION['userid']);
$thesisList = returnThesisList();

$search = ['#listConfer#', '#anketa#'];
$replace = [$thesisList, $personalData->username];

$personalArea = str_replace($search, $replace, $personalArea);

echo $personalArea;

echo '<script src="src/js/xhr4.js"></script>';

echo $OUTPUT->footer();


function returnThesisList(){
    global $DB, $USER;

    $result = '';

    $thesisList = $DB->getRecordsSql("select t.thesis_id, from_unixtime(date_app, '%d.%m.%Y') as dateapp, tt.thesistext,
                                        f.file_path
                                        from bls_thesis t
                                        inner join bls_thesis_type tt on t.id_thesis_type = tt.id_thesis_type
                                        left join bls_thesis_info ti on ti.id_thesis = t.thesis_id
                                        inner join bls_files f on t.file_id = f.id
                                        where t.user_id = $USER->id");


    foreach ($thesisList as $thesis){
        $result .= "Форма участия: $thesis->thesistext<br>";
        $result .= "Дата участия: $thesis->dateapp<br>";
        $result .= "Прикрепленный тезис:" . createLinkFile($thesis->file_path);
        $result .= "<br>";
    }

    return $result;
}