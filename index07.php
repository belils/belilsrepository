<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 22.09.2020
 * Time: 21:57
 */

require_once("src/php/config.php");
require_once("src/php/lib.php");

$OUTPUT->setTitle('О проекте');

echo $OUTPUT->header();


echo "<br><div class=\"conference\"><div class=\"conference__title\" style='text-align: center'>
                        <h4>МЕЖДУНАРОДНАЯ НАУЧНО-ПРАКТИЧЕСКАЯ КОНФЕРЕНЦИЯ ПО ВОПРОСАМ ПОДГОТОВКИ КАДРОВ ДЛЯ НАУЧНОГО ОБЕСПЕЧЕНИЯ АПК, ВКЛЮЧАЯ ВЕТЕРИНАРИЮ</h4>
                    </div></div>";

echo "<br><div class='conferens__time'> 
<div id=\"timer\" style=\"font-size: 30px;\" align=\"center\">
        <h4>До окончания регистрации</h4>
      <h1>
        <span id=\"day\">10</span>
        <span>д.</span>
        <span id=\"hour\">1</span>
        <span>ч.</span>
        <span id=\"minute\">2</span>
        <span>м.</span>
        <span id=\"second\">4</span>
        <span>с.</span>
      </h1>
</div>
</div>";

echo "<div class='conference'>
<div class='conferens__join' style = 'align:center'>
                        <button type='button' class='btn btn-outline-success btn1' onclick=
                        \"document.location='registration.php'\">Участовать</button>
                    </div></div>";


echo returnMainCard();

$listScienItem = $DB->getRecordsSql("select id, text from bls_scientific_interests");
    
echo "<div class='directions__items'>";

foreach($listScienItem as $item) {
    echo "<div class='directions__item'>$item->text</div>";
}

echo "</div>";

echo "<script src='src/js/timer.js'></script>";


echo $OUTPUT->footer();

//----------------------------------

function returnMainCard(){
    global $DB;

    //$cards = ['project', 'whom'];

    $cards = $DB->getRecordsSql("select id, type, title, text from bls_captions");
    $card_tempalate = file_get_contents("./src/template/card.html");

    $mainCard = '';

    foreach($cards as $card){
        $search = ['#name#', '#title#', '#text#'];
        $replace = [$card->type, "<h4 style='text-align:center'>$card->title</h4>", $card->text];
        $mainCard .= str_replace($search, $replace, $card_tempalate);
    }

    return $mainCard;
}