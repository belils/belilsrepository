<?php

require_once('src/php/config.php');

require_login();

$thesisForm = optional_param('thesisform', 1);

echo $OUTPUT->header();

$content = file_get_contents('src/template/29/thesis.html');

$content = replaceThesisForm($content);

$list = $DB->getRecordsSql('select id,text  from bls_scientific_interests where unused = 0');

$option = '';

foreach ($list as $item){
    $option .= "<option value='$item->id'>$item->text</option>";
}

$content = str_replace('#optionScienint#', $option, $content);

echo $content;

echo $OUTPUT->footer();


function replaceThesisForm($content){
    global $thesisForm;

    $content = str_replace('#thesisForm#', $thesisForm, $content);

    return $content;
}