<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 07.10.2020
 * Time: 2:39
 */
require_once('src/php/config.php');

$OUTPUT->setTitle('Контакты');

echo $OUTPUT->header();


$content = file_get_contents(($_COOKIE['lang']=='en')?'src/template/en/contact.html':'src/template/29/contact.html');

echo $content;

echo $OUTPUT->footer();