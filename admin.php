<?php
/**
 * Created by PhpStorm.
 * User: Takeonmars
 * Date: 28.09.2020
 * Time: 21:57
 */

require_once('src/php/config.php');

echo $OUTPUT->header();

if(has_capability('adminpanel:view')){

    $content = file_get_contents('src/template/29/admin.html');

    // Редактирование заголовков
    $listCaption = $DB->getRecordsSql("select id, type, title, text from bls_captions order by id");

    foreach($listCaption as $item){
        $content = str_replace('#' . $item->type . '#', $item->text, $content);
    }

//    $content = str_replace();

    $content = replaceScienint($content);
    $content = replaceConf($content);

    echo $content;

    echo "<script src='src/js/xhr5.js'></script>";


} else{
    echo "<h1>У вас нет доступа!</h1>";
}

echo $OUTPUT->footer();



//foreach($listCaption as $caption){
//    echo "<h2>$caption->title</h2>";
//    echo "<textarea name='$caption->type'>$caption->text</textarea>";
//    echo "<button>Сохранить</button>";
//}
//
//// Редактироване направлений
//echo "<h2>Направления</h2>";
//echo getTableScien();
//
//// Редактирование экспертов
//// Нахуй экспертов
//
//// Редактирование конференций
//echo "<h2></h2>";
//echo getConferenceTable();


function replaceScienint($content){
    global $DB;

    $template = file_get_contents('src/template/29/adminScienint.html');
    $listScien = $DB->getRecordsSql("select id, text, unused from bls_scientific_interests");

    $result = '';

    $search = [
        '#id#',
        '#scienintText#',
        '#checked#'
    ];

    foreach($listScien as $item){

        $replace = [
            $item->id,
            $item->text,
            ($item->unused==1)?'checked':''
        ];

        $result .= str_replace($search, $replace, $template);
    }

    return str_replace('#listScien#', $result, $content);
}

function replaceConf($content){
    global $DB;

    $template = file_get_contents('src/template/29/adminConf.html');
    $listConf = $DB->getRecordsSql("select conferences_id as id, year,
                                        from_unixtime(date_start, '%Y-%m-%d') as datestart, 
                                        from_unixtime(date_start_student, '%Y-%m-%d') as datestartstud
                                        from bls_conferences");

    $result = '';

    foreach($listConf as $item){
        $search = [
            '#year#',
            '#dateMain#',
            '#dateStud#'
        ];

        $replace = [
            $item->year,
            $item->datestart,
            $item->datestartstud
        ];
        $result .= str_replace($search, $replace, $template);
    }

    return str_replace('#confList#', $result, $content);
}

function getTableScien(){
    global $DB;

    $listScienint = $DB->getRecordsSql("select id, text, unused from bls_scientific_interests order by id");

    $table = "<table>";
    $table .= "<tr>
                <td>Не использовать</td>
                <td>Текст</td>
                <td>Действия</td>
               </tr>";

    foreach($listScienint as $scientint){

        $table .= "<tr>";
        $table .= "<td><input type='checkbox' name='used_$scientint->id' " . (($scientint->unused)?'checked':'') . "></td>";
        $table .= "<td><input type='text' name='scient_$scientint->id' value='$scientint->text'></td>";
        $table .= "<td><button>Удалить</button></td>";
        $table .= "</tr>";
    }

    $table .= "<tr>
                <td>#</td>
                <td><input type='text' name='scient_new'></td>
                <td><button>Добавить</button></td>
               </tr>";


    return $table . "</table>";
}

function getConferenceTable(){
    global $DB;

    $listConference = $DB->getRecordsSql("select conferences_id, year, from_unixtime(date_start, '%Y-%m-%d') as date_start, 
                                  from_unixtime(date_start_student, '%Y-%m-%d') as date_start_st from bls_conferences");

    $table = "<table>";
    $table .= "<tr>
                <td>Год</td>
                <td>Начало</td>
                <td>Начало для студентов</td>
                <td>Действия</td>
               </tr>";

    foreach($listConference as $conf){

        $table .= "<tr>";
        $table .= "<td>$conf->year</td>";
        $table .= "<td><input type='date' value='$conf->date_start'></td>";
        $table .= "<td><input type='date' value='$conf->date_start_st'></td>";
        $table .= "<td><button>Удалить</button></td>";
        $table .= "</tr>";
    }

    $table .= "<tr>
                <td>#</td>
                <td>#</td>
                <td><button>Добавить</button></td>
               </tr>";


    return $table . "</table>";
}

function printTabs($tabList){

    /*<div class="nav-tabs">
            <a href="#" data-tab="1" class="nav-tab active">Tab 1</a>
            <a href="#" data-tab="2" class="nav-tab">Tab 2</a>
            <a href="#" data-tab="3" class="nav-tab">Tab 3</a>
        </div>*/
    $content = "<div class='nav-tabs'>";
    foreach($tabList as $tab){
        $content .= "<span>$tab->text</span>";
    }
    return $content . '</div>';

}